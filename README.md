# hello-world

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
# Api url http://api.devzinho.com/api/

# store POST
```javaScript
endpoint: store

objct data: { 
            'key' => {your-key},
            'data' => {}
           }
```
# update PUT

```javaScript
endpoint: update/{key}

objct data: { 
            'data' => {id: {id-item}...}
           }
```
# edit GET
```javaScript
endpoint: edit/{key}
```

# show GET
```javaScript
endpoint: show/{key}
```
# delete GET
```javaScript
endpoint: delete/{key} or
endpoint: delete/{key}/{id}
```
